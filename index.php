<?php
include("config/connection.php");
include("func/func.php");
include("html/signIn.html");
$err = [];

if (isset($_POST['signin'])) {
    $login = $_POST['login'];
    $pass = $_POST['password'];
    if (empty($login))
        $err[] = 'Login cannot be empty!';
    if (empty($pass))
        $err[] = 'Password cannot be empty';
    if (count($err) > 0)
        echo showErrorMessage($err);
    else {
        $select = mysqli_query($con, "SELECT * FROM " . DB_TABLE . " WHERE login = '$login'");
        if ($select->num_rows == 0) {
            $err[] = 'Unfortunately login : <b>' . $login . '</b> not registered !';
            echo showErrorMessage($err);
        } else {
            $fields = $select->fetch_assoc();
            if (md5(md5($pass) . $fields['salt']) == $fields['passHash']) {

                $_SESSION["logged_user"] = $fields['login'];

                header('Location: welcome.php');
            } else {
                $err[] = 'Unfortunately password for : <b>' . $login . '</b> incorrect !';
                echo showErrorMessage($err);
            }
        }
    }
}
?>

