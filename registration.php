<?php
include 'config/connection.php';
include 'func/func.php';
include 'html/signUp.html';
$err = [];
if (isset($_POST['register'])) {
    $login = $_POST['login'];
    $pass = $_POST['password'];
    $pass_r = $_POST['password_r'];
    // check data
    if (empty($login)) {
        $err[] = 'Login cannot be empty!';
    }
    else {
        if (preg_match('/^[a-zA-Z]([a-zA-Z0-9_-]*)$/', trim($login)) === false) {
            if (strlen($login) < 4) {
                $err[] = 'Login too short at least 4 symbols' . "\n";
            } elseif (strlen($login) > 16) {
                $err[] = 'Login too long max size 16 symbols' . "\n";
            } else {
                $err[] = 'Login is incorrect' . "\n";
            }
        }
    }
    if (empty($pass))
        $err[] = 'Password cannot be empty';

    if (empty($pass_r))
        $err[] = 'Please fill password confirmation field ';

    if (count($err) > 0)
        echo $err;
    else {
        if ($pass != $pass_r)
            $err[] = 'Passwords do not match';

        if (count($err) > 0)
            echo showErrorMessage($err);
        else {
            $select = mysqli_query($con, "SELECT login FROM ".DB_TABLE ." WHERE login = '$login' LIMIT 1");
            if ($select->num_rows > 0)
                $err[] = 'Unfortunately login: <b>' . $login . '</b> not available !';
            if (count($err) > 0)
                echo showErrorMessage($err);
            else {
                //Get hash salt
                $salt = salt();
                //salt password
                $pass = md5(md5($pass) . $salt);
               $result =  mysqli_query($con, "insert into ".DB_TABLE."(login, passHash, salt)values('$login','$pass','$salt')");
                if ($result) {
                    $_SESSION['logged_user'] = $login;
                    header('Location: welcome.php');
                }

            }
        }
    }
}
?>
